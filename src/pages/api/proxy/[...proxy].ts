import { beApi } from '@/configs/ApiBe';
import { AxiosRequestConfig } from 'axios';
import type { NextApiRequest, NextApiResponse } from 'next';

export default async function handler(req: NextApiRequest, res: NextApiResponse) {
  const urls = req.url?.split('/api/proxy');
  const body = req.body;

  const config: AxiosRequestConfig<any> = {
    method: req.method,
    url: urls?.[1],
  };

  if (req.method === 'post') config.data = body;

  const result = await beApi(config);

  return res.status(result?.status ?? 200).json(result);
}
