import axios, { AxiosResponse, AxiosInstance, AxiosError, InternalAxiosRequestConfig } from 'axios';

let isRefreshing = false;
let failedQueue: any[] = [];

const APP_URL = process.env.NEXT_PUBLIC_APP_URL;
const errorMessages = 'Có lỗi trong quá trình thực thi';

const errorCallback = (status: number, error: string) => {
  return { status, error: error ?? errorMessages };
};

const processQueue = (error: AxiosError | null, token = null) => {
  failedQueue.forEach((item) => {
    if (error) {
      item.reject(error);
    } else {
      item.resolve(token);
    }
  });

  failedQueue = [];
};

const handleRefreshToken = async (config: InternalAxiosRequestConfig<any>, api: AxiosInstance) => {
  return new Promise((resolve, reject) => {
    failedQueue.push({ resolve, reject });
  })
    .then(() => {
      if (config) return api(config);
    })
    .catch((err) => {
      return Promise.reject(err);
    });
};

class ApiClient {
  baseURL: string;
  tokenType: string;

  constructor(baseURL = APP_URL, tokenType = 'Authorization') {
    this.baseURL = baseURL ?? '';
    this.tokenType = tokenType;
  }

  getInstance() {
    const api: AxiosInstance = axios.create({
      baseURL: this.baseURL,
      timeout: 30000,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      }
    });

    api.interceptors.request.use(
      (config: any) => {
        // const token = localStorage.getItem('token') ?? '';
        // if (config.headers) {
        //   config.headers[this.tokenType] = this.tokenType !== 'Authorization' ? token : `Bearer ${token}`;
        // }
        return config;
      },
      (error) => {
        return Promise.reject(error);
      }
    );

    api.interceptors.response.use(
      (response: AxiosResponse) => {
        return response.data;
      },
      async (error: AxiosError) => {
        const config = error.config;
        const resError = error.response;
        const dataError: any = resError?.data;

        if (!config) return;

        if (resError?.status === 401) {
          // if (config.url === `${REFRESH_URL}/api/auth/refresh_token`) {
          //   handleGetToken();
          //   return errorCallback(401, dataError?.message);
          // }

          // Handle if token is refreshing
          if (isRefreshing) {
            return handleRefreshToken(config, api);
          }
          isRefreshing = true;

          const res = await api.get(`${APP_URL}/auth/refresh_token`);

          if (res?.data?.access_token) {
            const { access_token } = res.data;
            localStorage.setItem('token', access_token);
            processQueue(null, access_token);
            if (config)
              return api(config).finally(() => {
                isRefreshing = false;
              });
          }

          return Promise.reject(error);
        }

        return errorCallback(500, dataError?.message);
      }
    );
    return api;
  }
}

export default ApiClient;

export const apiClient = new ApiClient(`${APP_URL}/api/proxy`).getInstance();
