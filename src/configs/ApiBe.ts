import axios, { AxiosResponse, AxiosInstance, AxiosError } from 'axios';

const API_URL = process.env.NEXT_PUBLIC_API_URL;
const errorMessages = 'Có lỗi trong quá trình thực thi';

const errorCallback = (status: number, error: string) => {
  return { status, error: error ?? errorMessages };
};

class ApiClient {
  baseURL: string;

  constructor(baseURL = API_URL) {
    this.baseURL = baseURL ?? '';
  }

  getInstance() {
    const api: AxiosInstance = axios.create({
      baseURL: this.baseURL,
      timeout: 30000,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    });

    api.interceptors.request.use(
      (config: any) => {
        return config;
      },
      (error) => {
        return Promise.reject(error);
      }
    );

    api.interceptors.response.use(
      (response: AxiosResponse) => {
        const data = response.data;

        if (data.success === false) {
          const message = data?.message ?? errorMessages;
          return { ...response.data, status: 400, error: message };
        }

        return response.data;
      },
      async (error: AxiosError) => {
        const resError = error.response;
        const dataError: any = resError?.data;

        return errorCallback(resError?.status ?? 200, dataError?.message);
      }
    );
    return api;
  }
}

export default ApiClient;

export const beApi = new ApiClient().getInstance();
