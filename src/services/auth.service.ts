import { apiClient } from "@/configs/ApiClient";
import { GET_USER } from "@/constants/endpoint";

export function getUser(username: string) {
  return apiClient.get(GET_USER, { params: { username } })
}